<?php

use Faker\Generator as Faker;

$autoIncrement = autoIncrement();

$factory->define(App\Monster::class, function (Faker $faker) use ($autoIncrement) {
    $autoIncrement->next();
    return [
        'name' => $faker->name,
        'rank' => $autoIncrement->current(),
        'img_src' => $faker->imageUrl($width = 200, $height = 200)
    ];
});

function autoIncrement()
{
    for ($i = 0; $i < 1000; $i++) {
        yield $i;
    }
}