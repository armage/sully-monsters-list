<?php

use Illuminate\Database\Seeder;

class MonstersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Monster::class, 5)->create();
    }
}
