/**
 * Initialize the app
 */
function startUp() {
    loaded = false;
    $("#boxes").empty();
    hideForm();
    counter();
    setCreateForm();
    resetForm();
    let listM = listMonsters();
    listM.done(function(monsters) {
        monsters.forEach(function(monster, index, array) {
            let mb = monsterControl(monster);
            $("#boxes").append(mb);
        });
        setDrag();
        pageLoaded(true);
    });
}
function pageLoaded(value) {
    if (value === true) {
        document.getElementById("main").style.display = "block";
        document.getElementById("loading").style.display = "none";
    } else {
        document.getElementById("main").style.display = "none";
        document.getElementById("loading").style.display = "block";
    }
}
/**
 * Generate the text for the counter div
 */
function counter() {
    $("#counter").empty();
    let total = getCounter();

    total.done(function(total) {
        let counterText;
        switch (total) {
            case 0:
                counterText = "There are no Monsters here";
                break;
            case 1:
                counterText = "We have only one Monster";
                break;
            default:
                counterText = "We have a total of " + total + " Monsters";
                break;
        }
        $("#counter").html(counterText);
    });
}

//
//
// Form functions
//
/**
 * Validate all the information at the update/create form
 */
function formValidate(action) {
    let errorShow = document.getElementById("errors");
    let monsterName = document.getElementById("monster_name");
    if (monsterName.value == "") {
        errorShow.innerHTML = "Please enter a name for the Monster";
        monsterName.focus();
        return false;
    }
    let monsterImage = document.getElementById("monster_image");
    if (action != "update" && monsterImage != null) {
        if (monsterImage.files[0] == null) {
            errorShow.innerHTML = "Please select an image for the Monster";
            return false;
        }
    }
    errorShow.innerHTML = "&nbsp;";
    return true;
}
/**
 * Clean of the members of the update/create form
 */
function resetForm() {
    document.getElementById("monster_name").value = "";
    document.getElementById("monster_image").value = "";
    document.getElementById("preview").innerHTML = "";
    $("#actionButton").off("click");
}
/**
 * Add Listener to image input to validate and preview image selected
 */
document.getElementById("monster_image").addEventListener("change", function() {
    let monsterPreview = document.getElementById("preview");
    let errorShow = document.getElementById("errors");
    errorShow.innerHTML = "&nbsp;";
    let files = this.files;
    let errors = "";

    if (!files) {
        errors += "File upload not supported by your browser.";
    }

    readImage(files[0], function(image) {
        if (files && files[0]) {
            //validate image dimension
            if (parseInt(image.width) > 320 || parseInt(image.heigh) > 320) {
                errors += "Images can't be bigger of 320x320\n";
            }
            //validate image format
            if (!/\.(png|jpeg|jpg|gif)$/i.test(files[0].name)) {
                errors += "Unsupported Image extension\n";
            }
        }

        if (errors) {
            //if errors show them
            errorShow.innerHTML = errors;
            document.getElementById("monster_image").value = null;
        } else {
            //if not errors show image
            monsterPreview.innerHTML = "";
            monsterPreview.appendChild(image);
        }
    });
});
/**
 * Load selected image
 * @param {file} file
 * @param {function} cb
 */
function readImage(file, cb) {
    let reader = new FileReader();
    reader.onload = function(e) {
        let image = new Image();
        image.onload = function() {
            cb(this);
        };
        image.src = reader.result;
    };
    reader.readAsDataURL(file);
}

//
//
// Monsters functions
//
/**
 * Remove the monster with the given id
 * @param {_id} id
 */
function removeMonster(id) {
    pageLoaded(false);
    let result = deleteMonster(id);
    result.done(function(response) {
        startUp();
    });
}
/**
 * Get the info from the form to create a new monster
 */
function addMonster() {
    pageLoaded(false);
    if (formValidate("create")) {
        let formData = new FormData();
        let file = document.getElementById("monster_image").files[0];
        formData.append("img_src", file);
        formData.append("name", document.getElementById("monster_name").value);

        let nextRankvar = nextRank();
        nextRankvar.done(function(text) {
            formData.append("rank", text);
            let endPost = postMonster(formData);
            endPost.done(function(response) {
                startUp();
            });
        });
    }
}
/**
 * Get the info from the form to update a monster
 */
function editMonster(id) {
    setUpdateForm(id);

    let monster = getMonster(id);
    monster.done(function(monster) {
        document.getElementById("monster_name").value = monster.name;

        let monsterPreview = document.getElementById("preview");
        let image = document.createElement("img");
        image.setAttribute("src", "/imgs/" + monster.img_src);
        monsterPreview.innerHTML = "";
        monsterPreview.appendChild(image);

        $("#monsterForm").show("blind", 500);
    });
}

function alterMonster(id) {
    pageLoaded(false);
    if (formValidate("update")) {
        let formData = new FormData();
        let file = document.getElementById("monster_image").files[0];

        if (file != null) {
            formData.append("img_src", file);
        }
        formData.append("name", document.getElementById("monster_name").value);
        let endPost = updateMonster(id, formData);
        endPost.done(function(response) {
            startUp();
        });
    }
}

function setUpdateForm(id) {
    $("#monsterForm")
        .addClass("updateForm")
        .removeClass("addForm");
    $("#actionButton").click(function() {
        alterMonster(id);
    });
    $("#actionButton").html("Update");
}
function setCreateForm() {
    $("#monsterForm")
        .addClass("addForm")
        .removeClass("updateForm");
    $("#actionButton").click(function() {
        addMonster();
    });
    $("#actionButton").html("Add");
}

function showCreateForm() {
    resetForm();
    setCreateForm();
    $("#monsterForm").show("blind", 500);
    $("#plusButton").hide();
    $("#minusButton").show();
}
function hideForm() {
    $("#monsterForm").hide("blind", 500);
    $("#plusButton").show();
    $("#minusButton").hide();
}
