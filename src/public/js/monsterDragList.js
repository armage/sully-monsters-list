let dragSrc = null;
let origEntityId = null;
let posi, posf;

function handleDragStart(e) {
    this.style.opacity = "0.4"; // this / e.target is the source node.
    origEntityId = this.getAttribute("entityid");

    dragSrc = this;
    posi = $(dragSrc).find("#rank")[0].innerHTML;
    //$(dragSrc).find("#rank")[0].innerHTML = "X";

    e.dataTransfer.effectAllowed = "move";
    e.dataTransfer.setData("text/html", this.innerHTML);
}

function handleDragOver(e) {
    if (e.preventDefault) {
        e.preventDefault(); // Necessary. Allows us to drop.
    }
    e.dataTransfer.dropEffect = "move"; // See the section on the DataTransfer object.
    return false;
}

function handleDragEnter(e) {
    this.classList.add("over"); // this / e.target is the current hover target.
}

function handleDragLeave(e) {
    this.classList.remove("over"); // this / e.target is previous target element.
}

function handleDrop(e) {
    // this / e.target is current target element.
    if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
    }

    // Don't do anything if dropping the same column we're dragging.
    if (dragSrc != this) {
        posf = $(this).find("#rank")[0].innerHTML;

        dragSrc.innerHTML = this.innerHTML;
        $(dragSrc).find("#rank")[0].innerHTML = posi;

        let movedEntityId = this.getAttribute("entityid");

        this.innerHTML = e.dataTransfer.getData("text/html");
        $(this).find("#rank")[0].innerHTML = posf;

        let monsterData = new FormData();
        monsterData.append("rank", posf);
        updateMonster(origEntityId, monsterData);
        this.setAttribute("entityid", origEntityId);
        monsterData.append("rank", posi);
        updateMonster(movedEntityId, monsterData);
        dragSrc.setAttribute("entityid", movedEntityId);
    }
    return false;
}

function handleDragEnd(e) {
    var boxes = document.querySelectorAll("#boxes .box");
    [].forEach.call(boxes, function(box) {
        box.classList.remove("over");
        box.style.opacity = "1";
    });
}

function setDrag() {
    var cols = document.querySelectorAll("#boxes .box");
    [].forEach.call(cols, function(col) {
        col.addEventListener("dragstart", handleDragStart, false);
        col.addEventListener("dragenter", handleDragEnter, false);
        col.addEventListener("dragover", handleDragOver, false);
        col.addEventListener("dragleave", handleDragLeave, false);
        col.addEventListener("drop", handleDrop, false);
        col.addEventListener("dragend", handleDragEnd, false);
    });
}
