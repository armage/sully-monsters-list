//
//
// AJAX functions to access the database
//
/**
 * Delete one monster from database
 * @param {_id} id
 */
function deleteMonster(id) {
    return $.ajax({
        url: "http://localhost:80/api/monsters/" + id,
        type: "DELETE",
        dataType: "json"
    });
}
/**
 * Update one monster from database
 * @param {_id} id
 * @param {obj} monster
 */
function updateMonster(id, formData) {
    return $.ajax({
        url: "http://localhost:80/api/monsters/" + id,
        type: "POST",
        processData: false,
        contentType: false,
        data: formData
    });
}
/**
 * Get the number of the next rank
 */
function nextRank() {
    return $.ajax({
        url: "http://localhost:80/api/monsters/nextRank",
        type: "GET",
        dataType: "json"
    });
}
/**
 * Post a new monster to database
 * @param {form} formData
 */
function postMonster(formData) {
    return $.ajax({
        url: "http://localhost:80/api/monsters",
        type: "POST",
        processData: false,
        contentType: false,
        data: formData
    });
}
/**
 * Get the list of all registered monsters
 */
function listMonsters() {
    return $.ajax({
        url: "http://localhost:80/api/monsters",
        type: "GET",
        dataType: "json"
    });
}
/**
 * Get data of one registered monsters
 * @param   {_id}   id
 */
function getMonster(id) {
    return $.ajax({
        url: "http://localhost:80/api/monsters/" + id,
        type: "GET",
        dataType: "json"
    });
}
/**
 * Get the amount of monsters at the database
 */
function getCounter() {
    return $.ajax({
        url: "http://localhost:80/api/monsters/count",
        type: "GET",
        dataType: "json"
    });
}
