function monsterControl(monster) {
    let image = document.createElement("img");
    image.setAttribute("src", "/imgs/" + monster.img_src);
    image.setAttribute("draggable", false);
    image.classList.add("monster-img");

    let div = document.createElement("div");
    div.classList.add("box");

    let h = document.createElement("header");
    h.innerHTML = monster.name;
    div.appendChild(h);

    let cr = document.createElement("div");
    cr.innerHTML = "rank";
    cr.classList.add("column");
    cr.setAttribute("id", "rank");
    cr.innerHTML = monster.rank;
    div.appendChild(cr);

    let ci = document.createElement("div");
    ci.classList.add("column");
    ci.setAttribute("draggable", false);
    ci.setAttribute("id", "image");
    ci.appendChild(image);
    div.appendChild(ci);

    let icons = document.createElement("div");
    icons.classList.add("iconsDiv");
    let pencil = document.createElement("i");
    pencil.classList.add("fas", "fa-2x", "fa-pencil-alt", "icon");
    pencil.addEventListener(
        "click",
        function() {
            editMonster(monster._id);
        },
        false
    );
    let trash = document.createElement("i");
    trash.classList.add("fas", "fa-2x", "fa-trash-alt", "icon");
    trash.addEventListener(
        "click",
        function() {
            removeMonster(monster._id);
        },
        false
    );

    icons.appendChild(pencil);
    icons.appendChild(trash);

    let co = document.createElement("div");
    co.appendChild(icons);
    co.classList.add("column");
    co.setAttribute("id", "options");
    div.appendChild(co);

    div.setAttribute("draggable", true);
    div.setAttribute("entityID", monster._id);

    return div;
}
