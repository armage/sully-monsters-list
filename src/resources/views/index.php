<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Monsters List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link
      rel="stylesheet"
      type="text/css"
      media="screen"
      href="/css/sully.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      media="screen"
      href="/css/all.min.css"
    />
    <script src="assets/jquery.min.js"></script>
    <script src="assets/jquery-ui.min.js"></script>

  </head> 
  <body>
                        <!-- <input type="hidden" name="_token" value="{{ csrf_token()}}"> -->

        <div class="container">
            <h1>Sully Monsters List</h1>
            <div id="loading" class="loader"></div>
            <div id="main">
                <div>
                    <label id="plusButton" onClick="showCreateForm()"><i class="fas fa-2x fa-plus-circle"></i></label>
                    <label id="minusButton" onClick="hideForm()"><i class="fas fa-2x fa-minus-circle"></i></label>
                    <div id="monsterForm" class="addForm">
                        <div class="row"><p id="errors">&nbsp;</p></div>
                        
                        <form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" onsubmit="return false;" >
                            <div class="row"><input type="text" id="monster_name" placeholder="Name"></div>
                            <div class="row">
                                <div class="half">
                                    <label for="monster_image">
                                        <i class="fas fa-cloud-upload-alt"></i> Select image
                                    </label>
                                    
                                </div>
                                <div class="half">
                                    <div id="preview"></div>
                                </div>
                            </div>
                            <button id="actionButton" class="btn" type="button">Add</button>
                            <input class="upload-btn-wrapper" type="file" name="file" id="monster_image"  />
                        </form>
                        
                    </div>
                </div>

                <div id="counter"></div>
                <div id="boxes"></div>
            </div>
        </div>

        <script src="/js/monsterDB.js"></script>
        <script src="/js/monsterControl.js"></script>
        <script src="/js/monsterDragList.js"></script>
        <script src="/js/monstersMain.js"></script>

    <script>
        

        $( document ).ready(function() {
            startUp();
            
        })
        
        

        


        

        
       


        
    </script>
  </body>
</html>
