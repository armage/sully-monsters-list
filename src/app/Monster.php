<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Monster extends Eloquent
{
    protected $fillable = ['name', 'rank', 'img_src'];

    public $timestamps = false;

    public $name;
    public $rank;
    public $img_src;
}