<?php

namespace App\Http\Controllers;

use App\Monster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MonsterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * Return the total of Monsters registered
     *
     * @return json
     */
    public function count()
    {
        return response()->json(Monster::count());
    }

    /**
     * list all monster
     *
     * @return json
     */
    public function showAllMonsters()
    {
        return response()->json(Monster::orderBy('rank', 'asc')->get());
    }

    /**
     * Show all the information of one Monster
     *
     * @param _id $id
     * @return json
     */
    public function showOneMonster($id)
    {
        return response()->json(Monster::find($id));
    }

    /**
     * Create a new Monster saving the image in the filesystem local
     *
     * @param Request $request
     * @return json
     */
    public function create(Request $request)
    {
        //Copy image to filesystem
        $file = $request->img_src;
        $filename = uniqid() . '-' . $file->getClientOriginalName();
        $path = $file->storeAs('local', $filename);

        $monster = new Monster([
            'name' => $request->name,
            'rank' => (int)$request->rank,
            'img_src' => $filename
        ]);
        $monster->save();

        return response()->json($monster, 201);
    }

    public function update($id, Request $request)
    {
        $monster = Monster::findOrFail($id);

        if (isset($request->rank)) {
            $monster["rank"] = (int)$request->rank;
        }
        if (isset($request->name)) {
            $monster["name"] = $request->name;
        }
        if (isset($request->img_src)) {
            Storage::disk('local')->delete('/local/' . $monster["img_src"]);
            $file = $request->img_src;
            $filename = uniqid() . '-' . $file->getClientOriginalName();
            $path = $file->storeAs('local', $filename);
            $monster["img_src"] = $filename;
        }
        $monster->save();

        return response()->json($request, 200);
    }

    public function delete($id)
    {
        //Fetch the monster to delete
        $monster = Monster::findOrFail($id);

        //Delete the image file
        Storage::disk('local')->delete('/local/' . $monster["img_src"]);

        //save the rank values previous delete
        $rankInit = (int)$monster["rank"];
        $last = (int)Monster::count();

        //Delete the record
        $result = $monster->delete();

        //Arrange list positions
        if ($result) {
            if ($rankInit < $last) {
                for ($i = $rankInit; $i < $last; $i++) {
                //Move the next monster one position up
                    Monster::where('rank', ($i + 1))->decrement('rank');
                }
            }
        }

        return response()->json('deleted', 200);
    }

    public function nextRank()
    {
        $nextRank = Monster::max('rank');
        $nextRank++;
        return response()->json($nextRank);
    }

}
