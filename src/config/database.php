<?php
return [
    'default' => 'mongodb',
    'connections' => [
        'mongodb' => [
            'driver' => 'mongodb',
            'host' => 'mongo',
            'port' => 27017,
            'database' => 'sully',
            'username' => 'root',
            'password' => 'example',
            'options' => [
                'database' => 'admin'
            ]
        ],
    ],
    'migrations' => 'migrations',
];