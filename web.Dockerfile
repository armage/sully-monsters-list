FROM php:7.2-apache
RUN a2enmod rewrite

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
RUN apt-get update
RUN apt-get -y install libcurl4-openssl-dev pkg-config libssl-dev curl git unzip zip nano 
WORKDIR /root
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer


RUN echo "Installing mongodb php extension..." && pecl install mongodb > /dev/null
RUN echo "extension=mongodb.so" >> `php --ini | grep "Loaded Configuration" | sed -e "s|.*:\s*||"`


WORKDIR /var/www/html/
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_NO_INTERACTION=1
#COPY ./src/composer.json ./src/composer.lock ./src/.env /var/www/html/
COPY ./src /var/www/html/

RUN composer install 
#--no-scripts --no-autoloader --no-suggest --no-ansi
RUN chmod 777 ./storage/app/local
RUN ln -s /var/www/html/storage/app/local /var/www/html/public/imgs